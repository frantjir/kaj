const VF = Vex.Flow;

const compositions = {
    compositions: [
        {
            tempo: "6/8",
            name: "Pro Elisku",
            staves: [
                {
                    notes: [
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "#", pause: false
                        },
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "#", pause: false
                        },
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "8r", accidental: "", pause: true
                        },

                        {
                            key: "C", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "8r", accidental: "", pause: true
                        },

                        {
                            key: "E", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "16", accidental: "#", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "8r", accidental: "", pause: true
                        }
                    ]
                },
                {
                    notes: [
                        {
                            key: "E", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "#", pause: false
                        },
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "#", pause: false
                        },
                        {
                            key: "E", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "8r", accidental: "", pause: true
                        },


                        {
                            key: "C", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "8r", accidental: "", pause: true
                        },


                        {
                            key: "E", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "B", height: "4", duration: "16", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "5", duration: "2r", accidental: "", pause: true
                        }
                    ]
                }
            ]
        },
        {
            tempo: "4/4",
            name: "Kocka leze dirou",
            staves: [
                {
                    notes: [
                        {
                            key: "C", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "2", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "A", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "2", accidental: "", pause: false
                        },

                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "4", accidental: "", pause: false
                        }
                    ]
                },
                {
                    notes: [
                        {
                            key: "D", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "G", height: "4", duration: "2", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "F", height: "4", duration: "8", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "E", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "D", height: "4", duration: "4", accidental: "", pause: false
                        },
                        {
                            key: "C", height: "4", duration: "2", accidental: "", pause: false
                        }
                    ]
                }
            ]
        }
    ]
};
