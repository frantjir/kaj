// listeners for piano keys
window.addEventListener('keydown', keyPressed);
window.addEventListener('keyup', keyUp);
window.addEventListener('mousedown', mousePressed);
window.addEventListener('mouseup', mouseUp);
const keyboardHolder = document.getElementsByClassName('keyboard-holder')[0];


const nameInput = document.getElementById('new-name');
const modalButtons = document.getElementsByClassName('modal-button');
let modalVisible = false;

const piano = new OnlinePiano();

// section of listener methods for pressing keys/clicking mouse

function pressed(key) {
    if(pressedKey[key.id] || modalVisible) {
        return false;
    }
    pressedKey[key.id] = true;
    key.classList.add('playing');

    playAudio(notes[key.id]);
}

function released(key) {
    let keys = document.querySelectorAll(".key");
    for (let i = 0; i < keys.length; i++) {
        keys[i].classList.remove('playing');
    }
    pressedKey[key.id] = false;
}

function keyUp(e) {
    let key = document.getElementById(e.keyCode);
    if (key) {
        released(key);
    }
}

function keyPressed(e) {
    // console.log(e);
    let key = document.getElementById(e.keyCode);
    if (key) {
        pressed(key);
    }
}

function mousePressed(event) {
    let element = event.target;
    if (element.classList.contains('key')) {
        pressed(element);
    }
    if (element.parentElement != null && element.parentElement.classList.contains('key')){
        pressed(element.parentElement);
    }
}

function mouseUp(event) {
    let element = event.target;
    if (element.classList.contains('key')) {
        released(element);
    }
    if (element.parentElement != null && element.parentElement.classList.contains('key')){
        released(element.parentElement);
    }
}

function playAudio(source) {
    let audio = new Audio(source);
    audio.play();
}

for (let i = 0; i < modalButtons.length; i++) {
    modalButtons[i].addEventListener('click', toggleModalState)
}

// show modal window or disable it
function toggleModalState () {
    modalVisible = !modalVisible;
    if (modalVisible) {
        document.body.classList.add('modal-visible');
        keyboardHolder.style.visibility = "hidden";
    } else {
        document.body.classList.remove('modal-visible');
        keyboardHolder.style.visibility = "visible";
    }
}

// check if input for new song name is empty
function checkIfEmpty() {
    let saveBtn = document.getElementsByClassName('save')[0];
    saveBtn.disabled = !nameInput.value.trim().length;
}

// save new song name typed to input
function saveNewName() {
    piano.saveNewName();
}



