// Storage classes - saving data for vexflow library (music sheet)
class Song {
    constructor(name, tempo) {
        this._staves = [];
        this._name = name;
        this._tempo = tempo;
    }

    get name() {
        return this._name;
    }

    get tempo() {
        return this._tempo;
    }

    get staves() {
        return this._staves;
    }

    set name(value) {
        this._name = value;
    }
}

class Stave {
    constructor() {
        this._notes = [];
    }

    get notes() {
        return this._notes;
    }
}

class Note {
    constructor(key, height, duration, accidental, pause) {
        this._key = key;
        this._height = height;
        this._duration = duration;
        this._accidental = accidental;
        this._pause = pause;
    }


    get key() {
        return this._key;
    }

    get height() {
        return this._height;
    }

    get duration() {
        return this._duration;
    }

    get accidental() {
        return this._accidental;
    }

    get pause() {
        return this._pause;
    }

}

class OnlinePiano {
    constructor() {
        this.songs = [];
        this.playing = false;
        this.playButton = document.getElementsByClassName('playButton')[0];
        this.stopButton = document.getElementsByClassName('stopButton')[0];

        // initializing VexFlow for music sheet
        const sheet = document.getElementById("sheet");
        const renderer = new VF.Renderer(sheet, VF.Renderer.Backends.SVG);
        renderer.resize(800, 300);
        this.context = renderer.getContext();
        this.setListeners();

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const selectedSong = urlParams.get('selected');
        this.loadSongs()
        this.loadComposition(selectedSong, false);
    }

    setListeners() {
        // loading file with song
        const fileRead = document.getElementById('file-read');
        fileRead.addEventListener('change', event => {
            const fileList = event.target.files[0];
            if (fileList) {
                let r = new FileReader();
                r.onload = function(e) {
                    let contents = e.target.result;
                    this.loadSong(JSON.parse(contents));
                }.bind(this)
                r.readAsText(fileList);
            } else {
                console.log("Error reading file.");
            }
        });

        this.playButton.addEventListener('click', function(e) {
            this.findSong();
        }.bind(this));

        this.stopButton.addEventListener('click', function(e) {
            this.playing = false;
            this.playButton.disabled = false;
            this.stopButton.disabled = true;
        }.bind(this));

        // walking through the browser history
        window.onpopstate = function(event) {
            this.loadComposition(event.state, true);
        }.bind(this);
    }

    // check if tone is pause
    isPause(tone) {
        let duration = tone['duration'];
        return isNaN(duration);
    }

    findSong() {
        let activeButton = document.getElementsByClassName('activeBtn');
        if (activeButton.length === 0) {
            return;
        }
        for (let i = 0; i < this.songs.length; i++) {
            if (this.songs[i].name === activeButton[0].textContent) {
                this.playSong(this.songs[i]);
            }
        }
    }

    // clear styles from key, so it is not shown pressed
    clearKeyStyles() {
        let keys = document.getElementsByClassName('key');
        for (let i = 0; i < keys.length; i++) {
            let key = keys[i];
            key.classList.remove('playing');
        }
    }

    // play 1 tone and call this function again after pause of specific duration (tone length)
    playTone(tones, position) {
        this.clearKeyStyles();
        if (!this.playing || position >= tones.length) {
            this.playing = false;
            this.playButton.disabled = false;
            return;
        }
        let tone = tones[position];
        let audioPath = 'Audio/';
        let accidental = '';
        if (tone.accidental === '#') {
            accidental = 'x';
        }
        let duration = tone.duration;
        if (!this.isPause(tone)) {
            // construct sound file name
            let note = tone.key + accidental + (tone.height - 1);
            this.pressKey(note);
            audioPath = audioPath + note + '.mp3';
            new Audio(audioPath).play();
        } else {
            duration = duration.slice(0, 1);
        }
        setTimeout(function () {
            this.playTone(tones, position + 1)
        }.bind(this), timings[duration]);
    }

    // set styles to key so it looks like pressed
    pressKey(note) {
        let key = sheetToKey[note];
        let keyElement = document.getElementById(key);
        keyElement.classList.add('playing');
    }

// take all notes from staves and play them
    playSong(song) {
        let staves = song.staves;
        let notes = [];
        for (let i = 0; i < staves.length; i++) {
            let staveNotes = staves[i].notes;
            for (let j = 0; j < staveNotes.length; j++) {
                notes.push(staveNotes[j]);
            }
        }
        this.playing = true;
        this.playButton.disabled = true;
        this.stopButton.disabled = false;
        this.playTone(notes, 0);
    }

    // load music sheet for selected song
    loadComposition(songIdx, history) {
        if (songIdx == null || songIdx >= this.songs.length) {
            songIdx = 0;
        }
        this.context.clear();
        let song = this.songs[songIdx];
        let staves = song.staves;
        for (let i = 0; i < staves.length; i++) {
            let VFStave = new VF.Stave(0, 100 * i, 800);
            VFStave.addClef("treble").addTimeSignature(song.tempo);
            VFStave.setContext(this.context).draw();
            let notes = staves[i].notes;
            let VFNotes = [];
            // fill staves with notes
            for (let j = 0; j < notes.length; j++) {
                let note = notes[j];
                let VFNote = new VF.StaveNote({
                    clef: "treble",
                    keys: [note.key + '/' + note.height],
                    duration: note['duration']
                });
                if (note.accidental.length !== 0) {
                    VFNote.addAccidental(0, new VF.Accidental(note.accidental));
                }
                VFNotes.push(VFNote);
            }
            // generate Beams for nicer look -- nozicky u not
            let beams = VF.Beam.generateBeams(VFNotes);
            VF.Formatter.FormatAndDraw(this.context, VFStave, VFNotes);
            for (let j = 0; j < beams.length; j++) {
                beams[j].setContext(this.context).draw();
            }
        }
        let h2 = document.getElementById("song-name");
        h2.innerText = song.name;

        if (!history) {
            this.setUrl(songIdx);
        }
        this.setActiveButton(songIdx);
    }

    // setting url address and adding it to history
    setUrl(idx) {
        let refresh = window.location.protocol + "//" + window.location.host + window.location.pathname + '?selected=' + idx;
        window.history.pushState(idx, '', refresh);
    }

    // set active song button more visible
    setActiveButton(buttonIdx) {
        let buttons = document.querySelectorAll(".compositions button");
        for (let i = 0; i < buttons.length; i++) {
            buttons[i].classList.remove('activeBtn');
            if (i == buttonIdx) {
                buttons[buttonIdx].classList.add('activeBtn');
            }
        }
    }

    // load song from json to objects
    loadSong(song) {
        let newSong = new Song(song['name'], song['tempo']);
        let staves = song['staves'];
        for (let j = 0; j < staves.length; j++) {
            let stave = staves[j];
            let newStave = new Stave();
            for (let k = 0; k < stave['notes'].length; k++) {
                let note = stave['notes'][k];
                let newNote = new Note(note['key'], note['height'], note['duration'], note['accidental'], note['pause']);
                newStave.notes.push(newNote);
            }
            newSong.staves.push(newStave);
        }
        this.addCompButton(newSong)
        this.songs.push(newSong);
    }

    // add new button with new song
    addCompButton(song) {
        let compElem = document.getElementsByClassName('compositions');
        let compButton = document.createElement('button');
        compButton.textContent = song.name;
        compButton.addEventListener('click', this.changeSheets.bind(this))
        compButton.addEventListener('dblclick', toggleModalState)
        compElem[0].appendChild(compButton);
    }

    // change sheets of music according to selected button
    changeSheets(event) {
        let songName = event.target.textContent;
        for (let i = 0; i < this.songs.length; i++) {
            if (this.songs[i].name === songName) {
                this.loadComposition(i, false);
                return;
            }
        }
    }

    loadSongs() {
        let comps = compositions['compositions'];
        for (let i = 0; i < comps.length; i++) {
            this.loadSong(comps[i]);
        }
    }

    saveNewName() {
        let h2 = document.getElementById("song-name");
        let oldName = h2.innerText;
        let newName = nameInput.value;
        let buttons = document.querySelectorAll(".compositions button");
        for (let i = 0; i < buttons.length; i++) {
            if (buttons[i].textContent === oldName) {
                buttons[i].textContent = newName;
            }
            if (this.songs[i].name === oldName) {
                this.songs[i].name = newName;
                this.loadComposition(i, false);
            }
        }
    }

}
