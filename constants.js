const sheetToKey = {
    "C3" : 81,
    "Cx3" : 50,
    "D3" : 87,
    "Dx3" : 51,
    "E3" : 69,
    "F3" : 82,
    "Fx3" : 53,
    "G3" : 84,
    "Gx3" : 54,
    "A3" : 89,
    "Ax3" : 55,
    "B3" : 85,

    "C4" : 90,
    "Cx4" : 83,
    "D4" : 88,
    "Dx4" : 68,
    "E4" : 67,
    "F4" : 86,
    "Fx4" : 71,
    "G4" : 66,
    "Gx4" : 72,
    "A4" : 78,
    "Ax4" : 74,
    "B4" : 77
};

const pressedKey = {
    81 : false,
    50 : false,
    87 : false,
    51 : false,
    69 : false,
    82 : false,
    53 : false,
    84 : false,
    54 : false,
    89 : false,
    55 : false,
    85 : false,

    90 : false,
    83 : false,
    88 : false,
    68 : false,
    67 : false,
    86 : false,
    71 : false,
    66 : false,
    72 : false,
    78 : false,
    74 : false,
    77 : false
};

const notes = {
    81 : 'Audio/C3.mp3',
    50 : 'Audio/Cx3.mp3',
    87 : 'Audio/D3.mp3',
    51 : 'Audio/Dx3.mp3',
    69 : 'Audio/E3.mp3',
    82 : 'Audio/F3.mp3',
    53 : 'Audio/Fx3.mp3',
    84 : 'Audio/G3.mp3',
    54 : 'Audio/Gx3.mp3',
    89 : 'Audio/A3.mp3',
    55 : 'Audio/Ax3.mp3',
    85 : 'Audio/B3.mp3',

    90 : 'Audio/C4.mp3',
    83 : 'Audio/Cx4.mp3',
    88 : 'Audio/D4.mp3',
    68 : 'Audio/Dx4.mp3',
    67 : 'Audio/E4.mp3',
    86 : 'Audio/F4.mp3',
    71 : 'Audio/Fx4.mp3',
    66 : 'Audio/G4.mp3',
    72 : 'Audio/Gx4.mp3',
    78 : 'Audio/A4.mp3',
    74 : 'Audio/Ax4.mp3',
    77 : 'Audio/B4.mp3'
};

const timings = {
    2 : 1300,
    4 : 650,
    8 : 325,
    16 : 165
};